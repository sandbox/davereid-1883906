<?php
/**
 * @file
 * node_reset_date_field_feature.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function node_reset_date_field_feature_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "entity_action_link" && $api == "entity_action_link_config") {
    return array("version" => "1");
  }
}

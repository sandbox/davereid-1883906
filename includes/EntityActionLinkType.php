<?php

abstract class EntityActionLinkType {
  protected $link = NULL;
  protected $entityType = NULL;
  protected $entity = NULL;

  public function __construct($link, $entity_type, $entity) {
    $this->link = $link;
    $this->entityType = $entity_type;
    $this->entity = $entity;
  }

  public static function listOptions($entity_type) {
    return array();
  }

  public function access() {
    return TRUE;
  }

  abstract public function label();

  abstract public function execute();
}

<?php

/**
 * @file
 * Defines the
 */

class RulesComponentEntityActionLinkType extends EntityActionLinkType {
  protected $rule = NULL;

  public function __construct($link, $entity_type, $entity) {
    parent::__construct($link, $entity_type, $entity);
    $this->rule = rules_config_load($this->link->action);
  }

  public static function listOptions($entity_type) {
    return rules_get_components(TRUE, 'action');
  }

  public function access() {
    if ($this->rule) {
      $modified_rule = clone $this->rule;
      $state = $modified_rule->setUpState(array($this->entity));
      return $modified_rule->conditionContainer()->evaluate($state);
    }
  }

  public function label() {
    return $this->rule->label();
  }

  public function execute() {
    // Duplicate code of rules_invoke_component().
    if ($component = rules_get_cache('comp_' . $this->link->action)) {
      return $component->executeByArgs(array($this->entity));
    }
    return FALSE;
  }
}

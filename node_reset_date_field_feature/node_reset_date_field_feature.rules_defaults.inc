<?php
/**
 * @file
 * node_reset_date_field_feature.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function node_reset_date_field_feature_default_rules_configuration() {
  $items = array();
  $items['rules_node_reset_date_field'] = entity_import('rules_config', '{ "rules_node_reset_date_field" : {
      "LABEL" : "Node reset date field",
      "PLUGIN" : "rule",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "node" : { "label" : "Node", "type" : "node" } },
      "IF" : [
        { "entity_has_field" : { "entity" : [ "node" ], "field" : "field_date" } },
        { "entity_is_of_type" : { "entity" : [ "node" ], "type" : "node" } }
      ],
      "DO" : [
        { "data_set" : { "data" : [ "node:field-date" ], "value" : "now" } },
        { "drupal_message" : {
            "message" : "Reset the date field on [node:title] to the current date and time.",
            "repeat" : 0
          }
        }
      ]
    }
  }');
  return $items;
}

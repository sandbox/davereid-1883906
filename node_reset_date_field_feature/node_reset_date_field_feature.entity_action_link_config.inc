<?php
/**
 * @file
 * node_reset_date_field_feature.entity_action_link_config.inc
 */

/**
 * Implements hook_default_entity_action_link_config().
 */
function node_reset_date_field_feature_default_entity_action_link_config() {
  $export = array();

  $config = new stdClass();
  $config->disabled = FALSE; /* Edit this to true to make a default config disabled initially */
  $config->api_version = 1;
  $config->machine_name = 'node_reset_date_field';
  $config->action_type = 'rule_component';
  $config->action = 'rules_node_reset_date_field';
  $config->entity_type = 'node';
  $config->view_mode = '';
  $config->weight = 0;
  $export['node_reset_date_field'] = $config;

  return $export;
}
